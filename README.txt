--- Menu Image Module ---


-- SUMMARY --

This module allows you to upload an image in the menu-item's configuration form 
and use the image anywhere in your theme.
This is useful if you want to display an image - that is related to a menu entry
 - somewhere in your theme (header, navigation).

The uploaded image is available at the template layer in a variable called "menu
_image" along with some meta data.
This module does not have any effect unless you know how to use the generated va
riable in your theme.

This module is Inspired by, similar to and initially based on Menu Icons, but it
 allows you to use the attached image anywhere in your theme, not only as a menu
 icon.


-- INSTALATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

 * Configure user permissions in admin/user/permissions menu_image module.
 
 * Customize Menu Image settings in Administer / Site Configuration / Menu
Image
 

-- USAGE --

After installing the module you can upload an image for a menu-item at
the menu items settings form.
The image is uploaded and saved according to the options specified.
The uploaded image can be used in all template files now using a variable called $menu_image:

<?php drupal_set_message('<pre>' . print_r($menu_image, TRUE) . '</pre>'); ?>

OR if you are using the developer module

<?php kpr($menu_image); ?>

To use the image anywhere in your template file:

<?php 
if ($menu_image['enable']) {
  print theme_image($menu_image['path'], $menu_image['descr'], $menu_image['descr']);
}
?>

 


-- CONTACT --
 
Current maintainer:

* Amin Alid (Anolim) - http://drupal.org/user/139023


This project has been sponsored by:

* Anolim.com
Anolim helps companies to reach new clients on the web and focuses on solutions
for online sales, marketing and communications, collaboration, knowledge
management, SaaS, e-commerce, content management systems and Drupal.
